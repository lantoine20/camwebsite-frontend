import Head from "next/head"
import { useRouter } from "next/router"
import React, { useEffect, useState } from "react"
import "react-responsive-carousel/lib/styles/carousel.min.css" // requires a loader
import { getProducts, getProduct } from "../../utils/api"
import { getStrapiMedia } from "../../utils/medias"
import ImageGallery from "react-image-gallery"
import "react-image-gallery/styles/css/image-gallery.css"

const ProductPage = ({ product }) => {
  const router = useRouter()
  const [size, setSize] = useState("")
  if (router.isFallback) {
    return <div>Loading product...</div>
  }
  function handleSizeChange(size) {
    setSize(size)
    {
      product.tailles
        .split("|")
        .map((taille) =>
          document.getElementById(taille).classList.remove("bg-gray-200")
        )
    }
    document.getElementById(size).classList.add("bg-gray-200")
  }

  const images = []
  for (let i = 0; i < product.carousel.length; i++) {
    let orig =
      "https://api.september-shop.fr" + product.carousel[i].formats.large.url
    let thumb =
      "https://api.september-shop.fr" + product.carousel[i].formats.large.url
    images.push({ original: orig, thumbnail: thumb })
  }

  return (
    <div className="m-6 grid grid-cols-1 sm:grid-cols-2 gap-6 mt-8">
      <Head>
        <title>{product.title} product</title>
      </Head>
      <div className="widthcarousel">
        <ImageGallery
          items={images}
          showPlayButton={false}
          showFullscreenButton={false}
        />
      </div>
      <div>
        <div className="flex items-center justify-between centered">
          <h1 className="text-2xl font-extrabold tracking-tight text-gray-900 sm:text-3xl">
            {product.title}
          </h1>
        </div>
        <div className="mt-4 lg:mt-0 lg:row-span-3">
          <div className="flex items-center justify-between centered">
            <p className="text-3xl text-gray-900">
              {product.price}
              <span className="text-lg inline-block align-top	">€</span>
            </p>
          </div>

          <div className="py-10 lg:pt-6 lg:pb-16 lg:col-start-1 lg:col-span-2 lg:border-r lg:border-gray-200 lg:pr-8">
            <div>
              <div className="flex items-center justify-between centered">
                <h3 className="text-sm text-gray-900 font-medium mb-2">
                  Description
                </h3>
              </div>

              <p className="text-base text-gray-900 has-text-center">
                {product.description}
              </p>
            </div>
          </div>
          <form className="mt-2">
            <div className="mt-2">
              <div className="flex items-center justify-between centered">
                <h3 className="text-sm text-gray-900 font-medium">Tailles</h3>
              </div>

              <fieldset className="mt-4 centered">
                <div className="grid grid-cols-3 gap-4 sm:grid-cols-8 lg:grid-cols-4">
                  {product.tailles.split("|").map((taille) => (
                    <div
                      key={taille}
                      id={taille}
                      className="group relative border rounded-md flex items-center py-3 px-2 justify-center text-sm font-medium uppercase bg-white sm:flex-1 sm:py-3 sm:px-2 shadow-sm text-gray-900 cursor-pointer"
                      onClick={() => {
                        handleSizeChange(taille)
                      }}
                    >
                      <p>{taille}</p>
                    </div>
                  ))}
                </div>
              </fieldset>
            </div>
            <div className="flex items-center justify-between centered mt-6">
              <button
                className="snipcart-add-item btn mt-6 ring-1 ring-gray-400 bg-white hover:bg-gray-200 cursor-pointer "
                data-item-id={product.id}
                data-item-price={product.price}
                data-item-url={router.asPath}
                data-item-description={product.description}
                data-item-image={getStrapiMedia(
                  product.image.formats.thumbnail.url
                )}
                data-item-name={product.title}
                data-item-custom1-name="Tailles"
                data-item-custom1-options={product.tailles}
                data-item-custom1-value={size}
                v-bind="customFields"
              >
                <p className="m-4 text-lg text-gray-600 px-2">
                  Ajouter au panier
                </p>
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default ProductPage

export async function getStaticProps({ params }) {
  const product = await getProduct(params.slug)
  return { props: { product } }
}

export async function getStaticPaths() {
  const products = await getProducts()
  return {
    paths: products.map((_product) => {
      return {
        params: { slug: _product.slug },
      }
    }),
    fallback: true,
  }
}
