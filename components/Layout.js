import Footer from "./Footer"
import Navbar from "./Navbar"

const Layout = ({ children, categories }) => {
  return (
    <div className="flex globalbg">
      <div className="flex flex-col min-h-screen w-full">
        <Navbar categories={categories} />
        <div className="flex-grow">{children}</div>
        <Footer />
      </div>
      <div
        hidden
        id="snipcart"
        data-api-key="M2Y0ZDdkZDctYTg4NS00MThiLTkwMmUtNWM4NjNiYjM2ZTgzNjM3NzE0NjgyOTk5MDkyMjA3"
      />
    </div>
  )
}

export default Layout
