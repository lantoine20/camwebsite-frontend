import NextImage from "./Image"
import Link from "next/link"

import { useRouter } from "next/router"
import { getStrapiMedia } from "../utils/medias"

const ProductsList = ({ products }) => {
  const router = useRouter()
  if (router.isFallback) {
    return <div>Loading product...</div>
  }

  return (
    <div className="main_content ">
      {products.map((_product) => (
        <Link href={`/products/${_product.slug}`} key={_product.slug}>
          <div className="card shadow-lg" key={_product.id}>
            <div className="card_img mt-6">
              {" "}
              <NextImage media={_product.image} />
            </div>
            <div className="card_header">
              <h2 className="text-2xl text-gray-800">{_product.title}</h2>
              <p className="text-3xl text-gray-800">
                {_product.price}
                <span className="text-xl inline-block align-top">€</span>
              </p>
            </div>
          </div>
        </Link>
      ))}
    </div>
  )
}

export default ProductsList
