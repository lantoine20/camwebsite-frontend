import Link from "next/link"

import { Fragment, useState } from "react"
import { Dialog, Popover, Tab, Transition } from "@headlessui/react"
import { MenuIcon, ShoppingBagIcon, XIcon } from "@heroicons/react/outline"

const Navbar = ({ categories = [] }) => {
  const [open, setOpen] = useState(false)

  function classNames(...classes) {
    return classes.filter(Boolean).join(" ")
  }

  return (
    <div className="navbarbg shadow-md">
      <Transition.Root show={open} as={Fragment}>
        <Dialog
          as="div"
          className="fixed inset-0 flex z-40 lg:hidden"
          onClose={setOpen}
        >
          <Transition.Child
            as={Fragment}
            enter="transition-opacity ease-linear duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="transition-opacity ease-linear duration-300"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <Transition.Child
            as={Fragment}
            enter="transition ease-in-out duration-300 transform"
            enterFrom="-translate-x-full"
            enterTo="translate-x-0"
            leave="transition ease-in-out duration-300 transform"
            leaveFrom="translate-x-0"
            leaveTo="-translate-x-full"
          >
            <div className="relative max-w-xs w-full bg-white shadow-xl pb-12 flex flex-col overflow-y-auto">
              <div className="px-4 pt-5 pb-2 flex">
                <button
                  type="button"
                  className="-m-2 p-2 rounded-md inline-flex items-center justify-center text-gray-400 "
                  onClick={() => setOpen(false)}
                >
                  <XIcon className="h-6 w-6 " aria-hidden="true" />
                </button>
              </div>

              {/* Links */}
              <Tab.Group as="div" className="mt-2">
                <div className="border-b border-gray-200">
                  <Tab.List className="-mb-px flex px-4 space-x-8">
                    <Tab
                      className={({ selected }) =>
                        classNames(
                          selected
                            ? "border-gray-400 text-gray-400"
                            : "text-gray-900 border-transparent",
                          "flex-1 whitespace-nowrap py-4 px-1 border-b-2 text-base font-medium"
                        )
                      }
                    >
                      Parcourir
                    </Tab>
                  </Tab.List>
                </div>
                <Tab.Panels as={Fragment}>
                  <Tab.Panel className="pt-10 pb-8 px-4 space-y-10">
                    <div>
                      <p className="font-medium text-gray-900 text-lg">
                        Vêtements
                      </p>
                      <ul role="list" className="mt-6 flex flex-col space-y-6">
                        <Link href="/">
                          <li
                            className="flow-root"
                            onClick={() => {
                              setOpen(false)
                            }}
                          >
                            <p className=" hover:text-gray-300 text-gray-800  cursor-pointer">
                              Tous les articles
                            </p>
                          </li>
                        </Link>
                        {categories.map((_category) => (
                          <Link
                            href={`/categories/${_category.slug}`}
                            key={_category.id}
                          >
                            <li
                              className="flow-root"
                              onClick={() => {
                                setOpen(false)
                              }}
                            >
                              <p className=" hover:text-gray-300 text-gray-800 cursor-pointer">
                                {_category.name}
                              </p>
                            </li>
                          </Link>
                        ))}
                      </ul>
                    </div>
                  </Tab.Panel>
                </Tab.Panels>
              </Tab.Group>
            </div>
          </Transition.Child>
        </Dialog>
      </Transition.Root>

      <header className="relative navbarbg">
        <nav
          aria-label="Top"
          className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8"
        >
          <div className="border-b border-gray-200">
            <div className="h-16 flex items-center">
              <button
                type="button"
                className="bg-white p-2 rounded-md text-gray-400 lg:hidden navbarbg"
                onClick={() => setOpen(true)}
              >
                <MenuIcon className="h-6 w-6 " aria-hidden="true" />
              </button>

              <div className="ml-4 flex lg:ml-0">
                <a href="/">
                  <img className="h-8 w-auto" src="/img/logo.png" alt="" />
                </a>
              </div>

              <Popover.Group className="hidden lg:ml-8 lg:block lg:self-stretch ">
                <div className="h-full flex space-x-8 ">
                  <Popover className="flex">
                    {({ open }) => (
                      <>
                        <div className="relative flex">
                          <Popover.Button
                            className={classNames(
                              open
                                ? "border-gray-400 text-gray-400 "
                                : "border-transparent text-gray-700 hover:text-gray-400 ",
                              "relative flex items-center transition-colors ease-out duration-200 text-lg font-medium border-b-2 -mb-px pt-px "
                            )}
                          >
                            Parcourir
                          </Popover.Button>
                        </div>

                        <Transition
                          as={Fragment}
                          enter="transition ease-out duration-200"
                          enterFrom="opacity-0"
                          enterTo="opacity-100"
                          leave="transition ease-in duration-150"
                          leaveFrom="opacity-100"
                          leaveTo="opacity-0"
                        >
                          <Popover.Panel className="absolute top-full inset-x-0 text-sm text-gray-500  popover">
                            <div
                              className="absolute inset-0 top-1/2 bg-white shadow"
                              aria-hidden="true"
                            />

                            <div className="relative navbarbg border-t border-gray-300">
                              <div className="max-w-7xl mx-auto px-8">
                                <div className="grid grid-cols-2 gap-y-4 gap-x-8 py-8">
                                  <div className="row-start-1 grid grid-cols-3 gap-y-2 gap-x-2 text-sm">
                                    <div>
                                      <p className="font-medium text-gray-800 text-lg">
                                        Vêtements
                                      </p>
                                      <ul
                                        role="list"
                                        className="mt-2 space-y-6 sm:mt-4 sm:space-y-4"
                                      >
                                        <Link href="/">
                                          <li
                                            className="flow-root"
                                            onClick={() => {
                                              setOpen(false)
                                            }}
                                          >
                                            <p className=" hover:text-gray-400 text-gray-700 cursor-pointer">
                                              Tous les articles
                                            </p>
                                          </li>
                                        </Link>

                                        {categories.map((_category) => (
                                          <Link
                                            href={`/categories/${_category.slug}`}
                                            key={_category.id}
                                          >
                                            <li
                                              className="flow-root"
                                              onClick={() => setOpen(true)}
                                            >
                                              <p className=" hover:text-gray-400 text-gray-700  cursor-pointer">
                                                {_category.name}
                                              </p>
                                            </li>
                                          </Link>
                                        ))}
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </Popover.Panel>
                        </Transition>
                      </>
                    )}
                  </Popover>
                </div>
              </Popover.Group>

              <div className="ml-auto flex place-items-center">
                <a
                  href="#"
                  className="py-2 px-2 font-medium text-gray-700 rounded  hover:text-gray-400 transition duration-300 snipcart-checkout text-lg  place-items-center"
                >
                  <i className="bx bx-shopping-bag font-semibold text-2xl"></i>
                  <span className="snipcart-total-price ml-2 font-semibold "></span>
                </a>
              </div>
            </div>
          </div>
        </nav>
      </header>
    </div>
  )
}

export default Navbar
