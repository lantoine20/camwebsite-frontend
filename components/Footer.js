import NextImage from "./Image"

const Footer = () => {
  return (
    <div className="flex justify-center m-6">
      <div className="flex has-items-center justify-items-center justify-center">
        <p className="text-lg font-semibold text-gray-400 ">
          {" "}
          <i className="bx bx-copyright font-semibold text-sm"></i>
          <span>September</span>
        </p>
      </div>
      <div className="flex gap-3 ml-4 text-gray-400 ">
        <a href="https://twitter.com/strapijs" className="max-w-xs ml-4">
          <i className="bx bxl-facebook font-semibold text-2xl hover:text-black"></i>
        </a>
        <a href="https://facebook.com/strapijs" className="ml-3">
          <i className="bx bxl-instagram font-semibold text-2xl hover:text-black"></i>
        </a>
        <a
          href="https://github.com/strapi/strapi-starter-next-ecommerce"
          className="ml-3"
        >
          <i className="bx bxl-twitter font-semibold text-2xl hover:text-black"></i>
        </a>
      </div>
    </div>
  )
}

export default Footer
